from django.contrib import admin
from .models import BugReport, FeatureRequest


# Класс администратора для модели BugReport
@admin.register(BugReport)
class BugReportAdmin(admin.ModelAdmin):
    list_display = ('title', 'project', 'task', 'priority','status', 'created_at', 'updated_at')
    list_filter = ('status', 'priority', 'project', 'task')
    search_fields = ('title', 'description')

    def updatestatus(self, request, queryset, newstatus):
        updated = queryset.update(status=newstatus)
        if updated == 1:
            self.message_user(request, f'{updated} Bug Report marked as {newstatus}')
        else:
            self.message_user(request, f'{updated} Bug Reports marked as {newstatus}')

    def mark_as_new(self, request, queryset):
        self.updatestatus(request, queryset, 'New')
    mark_as_new.short_description = "Mark selected Bug Reports as New"

    def mark_as_in_progress(self, request, queryset):
        self.updatestatus(request, queryset, 'In_progress')
    mark_as_in_progress.short_description = "Mark selected Bug Reports as In Progress"

    def mark_as_completed(self, request, queryset):
        self.updatestatus(request, queryset, 'Completed')
    mark_as_completed.short_description = "Mark selected Bug Reports as Completed"

    actions = mark_as_new, mark_as_in_progress, mark_as_completed


# Класс администратора для модели FeatureRequest
@admin.register(FeatureRequest)
class FeatureRequestAdmin(admin.ModelAdmin):
    list_display = ('title', 'project', 'task', 'priority', 'created_at', 'updated_at')
    list_filter = ('status', 'priority', 'project', 'task')
    search_fields = ('title', 'description')
