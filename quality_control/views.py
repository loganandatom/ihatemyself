from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from .models import BugReport, FeatureRequest
from django.views import View
from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from .forms import BugReportForm, FeatureRequestForm
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy


class IndexView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'quality_control/index.html')


def bug_list(request):
    bugs = BugReport.objects.all()
    return render(request, 'quality_control/bug_list.html', {'bug_list': bugs})


class BugReportListView(ListView):
    model = BugReport
    template_name = 'quality_control/bug_list.html'


class BugReportDetailView(DetailView):
    model = BugReport
    pk_url_kwarg = 'bug_id'
    template_name = 'quality_control/bug_detail.html'


def bug_detail(request, bug_id):
    bug = get_object_or_404(BugReport, id=bug_id)
    return render(request, 'quality_control/bug_detail.html', {'bug': bug})


def feature_list(request):
    features = FeatureRequest.objects.all()
    return render(request, 'quality_control/feature_list.html', {'feature_list': features})


class FeatureRequestListView(ListView):
    model = FeatureRequest
    template_name = 'quality_control/feature_list.html'


class FeatureRequestDetailView(DetailView):
    model = FeatureRequest
    pk_url_kwarg = 'feature_id'
    template_name = 'quality_control/feature_detail.html'


def feature_detail(request, feature_id):
    feature = get_object_or_404(FeatureRequest, id=feature_id)
    return render(request, 'quality_control/feature_detail.html', {'feature': feature})


def create_bug_report(request):
    if request.method == 'POST':
        form = BugReportForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('quality_control:bug_list')
    else:
        form = BugReportForm()
    return render(request, 'quality_control/bug_report_form.html', {'form': form})


class BugReportCreateView(CreateView):
    model = BugReport
    form_class = BugReportForm
    template_name = 'quality_control/bug_report_form.html'
    success_url = reverse_lazy('quality_control:bug_list')


def create_feature_request(request):
    if request.method == 'POST':
        form = FeatureRequestForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('quality_control:feature_list')
    else:
        form = FeatureRequestForm()
    return render(request, 'quality_control/feature_request_form.html', {'form': form})


class FeatureRequestCreateView(CreateView):
    model = FeatureRequest
    form_class = FeatureRequestForm
    template_name = 'quality_control/feature_request_form.html'
    success_url = reverse_lazy('quality_control:feature_list')


def update_bug_report(request, bug_id):
    bug = get_object_or_404(BugReport, pk=bug_id)
    if request.method == 'POST':
        form = BugReportForm(request.POST, instance=bug)
        if form.is_valid():
            form.save()
            return redirect('quality_control:bug_detail', bug_id=bug.id)
    else:
        form = BugReportForm(instance=bug)
    return render(request, 'quality_control/bug_update.html', {'form': form, 'bug': bug})


class BugReportUpdateView(UpdateView):
    model = BugReport
    form_class = BugReportForm
    template_name = 'quality_control/bug_update.html'
    pk_url_kwarg = 'bug_id'
    success_url = reverse_lazy('quality_control:bug_list')


def update_feature_request(request, feature_id):
    feature = get_object_or_404(FeatureRequest, pk=feature_id)
    if request.method == 'POST':
        form = FeatureRequestForm(request.POST, instance=feature)
        if form.is_valid():
            form.save()
            return redirect('quality_control:feature_detail', feature_id=feature.id)
    else:
        form = FeatureRequestForm(instance=feature)
    return render(request, 'quality_control/feature_update.html', {'form': form, 'feature': feature})


class FeatureRequestUpdateView(UpdateView):
    model = FeatureRequest
    form_class = FeatureRequestForm
    template_name = 'quality_control/feature_update.html'
    pk_url_kwarg = 'feature_id'
    success_url = reverse_lazy('quality_control:feature_list')


def delete_bug(request, bug_id):
    bug = get_object_or_404(BugReport, pk=bug_id)
    bug.delete()
    return redirect('quality_control:bug_list')


class BugReportDeleteView(DeleteView):
    model = BugReport
    pk_url_kwarg = 'bug_id'
    success_url = reverse_lazy('quality_control:bug_list')
    template_name = 'quality_control/bug_confirm_delete.html'


def delete_feature(request, feature_id):
    feature = get_object_or_404(FeatureRequest, pk=feature_id)
    feature.delete()
    return redirect('quality_control:feature_list')


class FeatureRequestDeleteView(DeleteView):
    model = FeatureRequest
    pk_url_kwarg = 'feature_id'
    success_url = reverse_lazy('quality_control:feature_list')
    template_name = 'tasks/feature_confirm_delete.html'

# def index(request):
#     bug_list_url = reverse('quality_control:bug_list')
#     feature_list_url = reverse('quality_control:feature_list')
#     html = f"<h1>Система контроля качества</h1><a href='{bug_list_url}'>Список всех багов</a></br><a href='{feature_list_url}'>Список всех запросов на улучшение</a>"
#     return HttpResponse(html)

# class IndexView(View):
#     def get(self, request, *args, **kwargs):
#         bug_list_url = reverse('quality_control:bug_list')
#         feature_list_url = reverse('quality_control:feature_list')
#         html = f"<h1>Система контроля качества</h1><a href='{bug_list_url}'>Список всех багов</a></br><a href='{feature_list_url}'>Список всех запросов на улучшение</a>"
#         return HttpResponse(html)

# def bug_list(request):
#     bugs = BugReport.objects.all()
#     bugs_html = '<h1>Cписок отчетов об ошибках</h1><ul>'
#     for bug in bugs:
#         bugs_html += f'<li><a href="{bug.id}/">{bug.title}</a><a> - {bug.status}</a></li>'
#     bugs_html += '</ul>'
#     return HttpResponse(bugs_html)


# class BugReportDetailView(DetailView):
#     model = BugReport
#     pk_url_kwarg = 'bug_id'
#
#     def get(self, request, *args, **kwargs):
#         self.object = self.get_object()
#         bug = self.object
#         task = bug.task
#         project = bug.project
#         project_list_url = reverse('tasks:projects_list')
#         tasks_list_url = reverse('tasks:index')
#         response_html = f'<h1>{bug.title}</h1><p>Description: {bug.description}</p><p>Status: {bug.status}</p><p>Priority: {bug.priority}</p>'
#         response_html += '<h2>Проект</h2><ul>'
#         response_html += f'<li><a href="{project_list_url}{project.id}/">{project.name}</a></li>'
#         response_html += '</ul>'
#         response_html += '<h2>Задача</h2><ul>'
#         response_html += f'<li><a href="{project_list_url}{project.id}{tasks_list_url}{task.id}/">{task.name}</a></li>'
#         response_html += '</ul>'
#         return HttpResponse(response_html)


# def feature_list(request):
#     features = FeatureRequest.objects.all()
#     features_html = '<h1>Список запросов на улучшение</h1><ul>'
#     for feature in features:
#         features_html += f'<li><a href="{feature.id}/">{feature.title}</a><a> - {feature.status}</a></li>'
#     features_html += '</ul>'
#     return HttpResponse(features_html)


# class FeatureRequestDetailView(DetailView):
#     model = FeatureRequest
#     pk_url_kwarg = 'feature_id'
#
#     def get(self, request, *args, **kwargs):
#         self.object = self.get_object()
#         feature = self.object
#         task = feature.task
#         project = feature.project
#         project_list_url = reverse('tasks:projects_list')
#         tasks_list_url = reverse('tasks:index')
#         response_html = f'<h1>{feature.title}</h1><p>Description: {feature.description}</p><p>Status: {feature.status}</p><p>Priority: {feature.priority}</p>'
#         response_html += '<h2>Проект</h2><ul>'
#         response_html += f'<li><a href="{project_list_url}{project.id}/">{project.name}</a></li>'
#         response_html += '</ul>'
#         response_html += '<h2>Задача</h2><ul>'
#         response_html += f'<li><a href="{project_list_url}{project.id}{tasks_list_url}{task.id}/">{task.name}</a></li>'
#         response_html += '</ul>'
#         return HttpResponse(response_html)
